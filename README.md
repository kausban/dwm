# My DWM Fork

DWM ([suckless.org](https://dwm.suckless.org/)) 6.3-1 with following patches:

1. [bottomstack](https://dwm.suckless.org/patches/bottomstack/dwm-bottomstack-6.1.diff)
2. [gaplessgrid](https://dwm.suckless.org/patches/gaplessgrid/dwm-gaplessgrid-6.1.diff)
3. [pertag](https://dwm.suckless.org/patches/pertag/dwm-pertag-6.2.diff)
4. [scratchpad](https://dwm.suckless.org/patches/scratchpad/dwm-scratchpad-6.2.diff)
5. [attachaside](https://dwm.suckless.org/patches/attachaside/dwm-attachaside-6.3.diff)
6. [dwm-uselessgap-20211119-58414bee958f2.diff](https://dwm.suckless.org/patches/uselessgap/dwm-uselessgap-20211119-58414bee958f2.diff)
7. coloremoji - needs libxft with bgra glyph patches [[aur](https://aur.archlinux.org/packages/libxft-bgra/)]
8. swallow - [dwm-swallow-20201211-61bb8b2.diff](https://dwm.suckless.org/patches/swallow/dwm-swallow-20201211-61bb8b2.diff)
9. systray - editted version of [dwm-systray-6.3](https://dwm.suckless.org/patches/systray/dwm-systray-6.3.diff)
