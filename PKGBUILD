# Modified: kausban <mail at kausban com>
# Maintainer: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Dag Odenhall <dag.odenhall@gmail.com>
# Contributor: Grigorios Bouzakis <grbzks@gmail.com>

groups=('modified')
pkgname=dwm
pkgver=6.3
pkgrel=1
pkgdesc="A dynamic window manager for X"
url="https://dwm.suckless.org"
arch=('i686' 'x86_64')
license=('MIT')
options=(zipman)
depends=('libx11' 'libxinerama' 'libxft' 'freetype2' 'st' 'dmenu')
install=dwm.install
source=(https://dl.suckless.org/dwm/dwm-$pkgver.tar.gz
    dwm-bottomstack-6.1.diff
    dwm-gaplessgrid-6.1.diff
    dwm-pertag-6.2.diff
    dwm-scratchpad-6.2.diff
    dwm-attachaside-6.3.diff
    dwm-uselessgap-20211119-58414bee958f2.diff
    dwm-coloremoji-6.1.diff
    dwm-systray-6.3_edit.diff
    dwm-swallow-20201211-61bb8b2.diff
    config.h
    dwm.desktop)
md5sums=('ed3aa40b97e29dbbe7d7d3f296bc2fcc'
         '14b03ce4f39b4d5f2d52302c172b3617'
         'a298c7e6e69134f24bca88016e3463ce'
         '630051fbd9750a4344c599eb42bf69fb'
         '6c0f61033ad60147ca8d420b434d818d'
         'b851c4a1727ee2d97d678462000bea35'
         'ac1e8f7878aa3b817aae40cc31f6a4df'
         'd39b25e8ede39815ffc8e2e3044ab24e'
         '8b2aa99cfaa1e7034d7eb8eae66a4c5b'
         'f6c3e4ffd2b385e2f19173d28acc2bd7'
         'cf7ef77d15c15234eb0207a4e36d923c'
         '939f403a71b6e85261d09fc3412269ee')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  cp "$srcdir/config.h" config.h
  patch -p1 -i "${srcdir}/dwm-bottomstack-6.1.diff"
  patch -p1 -i "${srcdir}/dwm-gaplessgrid-6.1.diff"
  patch -p1 -i "${srcdir}/dwm-pertag-6.2.diff"
  patch -p1 -i "${srcdir}/dwm-scratchpad-6.2.diff"
  patch -p1 -i "${srcdir}/dwm-attachaside-6.3.diff"
  patch -p1 -i "${srcdir}/dwm-uselessgap-20211119-58414bee958f2.diff"
  patch -p1 -i "${srcdir}/dwm-coloremoji-6.1.diff"
  patch -p1 -i "${srcdir}/dwm-swallow-20201211-61bb8b2.diff"
  patch -p1 -i "${srcdir}/dwm-systray-6.3_edit.diff"  
}

build() {
  cd "$srcdir/$pkgname-$pkgver"
  make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11 FREETYPEINC=/usr/include/freetype2
}

package() {
  cd "$srcdir/$pkgname-$pkgver"
  make PREFIX=/usr DESTDIR="$pkgdir" install
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 README "$pkgdir/usr/share/doc/$pkgname/README"
  install -Dm644 "$srcdir/dwm.desktop" "$pkgdir/usr/share/xsessions/dwm.desktop"
}
